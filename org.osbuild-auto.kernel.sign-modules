#!/usr/bin/python3
"""
Sign kernel and modules
"""

import os
import pathlib
import subprocess
import sys
import tempfile

import osbuild.api
from osbuild.util import parsing

SCHEMA_2 = """
"options": {
  "additionalProperties": false,
  "required": ["key", "x509"],
  "properties": {
    "key": {
      "description": "Path to the signing key.",
      "type": "string"
    },
    "x509": {
      "description": "Path to the signing certificate.",
      "type": "string"
    }
  }
},
"inputs": {
  "type": "object",
  "additionalProperties": true
}
"""


def sign_module(signfile, mod, key, x509):
    algs = (
        {"hdr": b'\x7fELF',
         "inflate": ["cat"], "deflate": ["cat"]},
        {"hdr": b'(\xb5/\xfd',
         "inflate": ["unzstd", "-c"], "deflate": ["zstd", "-f", "-c"]},
    )
    alg = {}

    with open(mod, "rb") as f:
        hdr = f.read(4)  # 4-bytes seems to be the largest header size
        for a in algs:
            if a["hdr"] == hdr[:len(a["hdr"])]:
                alg = a
                break
    if not alg:
        raise ValueError(f"Unrecognized module file format for {mod}.")

    with tempfile.NamedTemporaryFile() as tmp:
        subprocess.run(alg["inflate"] + [f"{mod}"],
                       encoding="utf8", stdout=tmp, input=None, check=True)
        tmp.flush()
        subprocess.run([signfile, "sha256", key, x509, tmp.name],
                       encoding="utf8", stdout=sys.stderr, input=None,
                       check=True)
        with open(mod + ".signed", "wb") as f:
            subprocess.run(alg["deflate"] + [f"{tmp.name}"],
                           encoding="utf8", stdout=f, input=None, check=True)
        os.rename(mod + ".signed", mod)


def main(args, options):
    tree = args["tree"]
    key = parsing.parse_location(options["key"], args)
    x509 = parsing.parse_location(options["x509"], args)

    moddir = os.path.join(tree, "usr/lib/modules")

    for kver in os.listdir(moddir):
        if not os.path.isdir(os.path.join(moddir, kver)):
            continue

        signfile = os.path.join("/usr/src/kernels", kver, "scripts/sign-file")
        kmoddir = os.path.join(moddir, kver)

        for mod in pathlib.Path(kmoddir).rglob("*.ko*"):
            sign_module(signfile, str(mod), key, x509)


if __name__ == '__main__':
    _args = osbuild.api.arguments()
    r = main(_args, _args["options"])
    sys.exit(r)
