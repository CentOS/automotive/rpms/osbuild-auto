#!/usr/bin/python3
"""Sign a commit in an ostree repo

Given an ostree commit (referenced by a ref) in a repo and an ed25519
secret key this adds a signature to the commit detached metadata.
This commit can then be used to validate the commit, during ostree
pull, during boot, or at any other time.

"""

import base64
import os
import subprocess
import sys

from osbuild import api

SCHEMA_2 = """
"options": {
  "additionalProperties": false,
  "required": ["repo", "ref", "key"],
  "properties": {
    "repo": {
      "description": "Location of the OSTree repo.",
      "type": "string"
    },
    "ref": {
      "description": "OStree ref to create for the commit",
      "type": "string",
      "default": ""
    },
    "key": {
      "description": "Path to the secret key",
      "type": "string"
    }
  }
}
"""


def ostree(*args, _input=None, **kwargs):
    args = list(args) + [f'--{k}={v}' for k, v in kwargs.items()]
    print("ostree " + " ".join(args), file=sys.stderr)
    subprocess.run(["ostree"] + args,
                   encoding="utf8",
                   stdout=sys.stderr,
                   input=_input,
                   check=True)

def main(tree, options):
    repo = os.path.join(tree, options["repo"].lstrip("/"))
    ref = options["ref"]
    keyfile = os.path.join(tree, options["key"].lstrip("/"))

    ostree("sign", f"--repo={repo}", f"--keys-file={keyfile}", ref)

if __name__ == '__main__':
    stage_args = api.arguments()

    r = main(stage_args["tree"],
             stage_args["options"])

    sys.exit(r)
