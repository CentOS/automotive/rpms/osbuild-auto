Name:           osbuild-auto
Version:        0.1.14
Release:        1%{?dist}
Summary:        Automotive stages for osbuild

License:        GPLv2+
Source1:	org.osbuild-auto.ostree.deploy-verity
Source2:	org.osbuild-auto.ostree.config
Source3:	org.osbuild-auto.ostree.genkey
Source4:	org.osbuild-auto.ostree.sign
Source5:	org.osbuild-auto.aboot.conf
Source6:	org.osbuild-auto.aboot.update
Source7:	org.osbuild-auto.write-device
Source8:        org.osbuild-auto.ostree.config-compliance-mode
Source9:	org.osbuild-auto.initoverlayfs.conf
Source10:	org.osbuild-auto.ostree.pre-gen
Source11:	org.osbuild-auto.kernel.add-cert
Source12:	org.osbuild-auto.kernel.genkey
Source13:	org.osbuild-auto.kernel.sign-modules
Source14:	org.osbuild-auto.kernel.remove-modules

Requires:       osbuild
BuildArch:      noarch

%description
Automotive stages for osbuild

%prep
rm -rf %{name}-{%version}
mkdir %{name}-{%version}

%build
cd %{name}-{%version}

%install
cd %{name}-{%version}
mkdir -p %{buildroot}%{_prefix}/lib/osbuild/stages
install -m755 %{SOURCE1} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.ostree.deploy-verity
install -m755 %{SOURCE2} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.ostree.config
install -m755 %{SOURCE3} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.ostree.genkey
install -m755 %{SOURCE4} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.ostree.sign
install -m755 %{SOURCE5} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.aboot.conf
install -m755 %{SOURCE6} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.aboot.update
install -m755 %{SOURCE7} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.write-device
install -m755 %{SOURCE8} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.ostree.config-compliance-mode
install -m755 %{SOURCE9} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.initoverlayfs.conf
install -m755 %{SOURCE10} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.ostree.pre-gen
install -m755 %{SOURCE11} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.kernel.add-cert
install -m755 %{SOURCE12} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.kernel.genkey
install -m755 %{SOURCE13} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.kernel.sign-modules
install -m755 %{SOURCE14} %{buildroot}%{_prefix}/lib/osbuild/stages/org.osbuild-auto.kernel.remove-modules

%files
%{_prefix}/lib/osbuild/stages/*

%changelog
* Tue Jan 14 2025 Alexander Larsson <alexl@redhat.com> - 0.1.14-1
- Add kernel.remove-modules stage

* Wed Oct 9 2024 Ian Mullins <imullins@redhat.com> - 0.1.13-1
- Bump version to 0.1.13
- kernel: avoid using NETLINK_CRYPTO for hmac

* Thu Oct 3 2024 Ian Mullins <imullins@redhat.com> - 0.1.12-1
- Bump version to 0.1.12
- kernel: support overwriting existing keyring certificates

* Tue Aug 13 2024 Ian Mullins <imullins@redhat.com> - 0.1.11-1
- Bump version to 0.1.11

* Wed Jul 17 2024 Martin McConnell <mmcconne@redhat.com> - 0.1.10-1
- Add compress_kernel argument

* Fri Jun 28 2024 Alexander Larsson <alexl@redhat.com> - 0.1.9-1
- Add kernel signature stages

* Thu Apr 25 2024 Alexander Larsson <alexl@redhat.com> - 0.1.8-1
- Add ostree.pre-gen stage

* Tue Apr 23 2024 Eric Curtin <ecurtin@redhat.com> - 0.1.7-1
- Add vbmeta support

* Fri Apr 5 2024 Douglas Schilling Landgraf <dougsland@redhat.com> - 0.1.6-1
- Add initoverlayfs.conf

* Wed Aug 16 2023 Alexander Larsson <alexl@redhat.com> - 0.1.5-1
- Update ostree stages to use base64 keys

* Tue Jul 18 2023 Eric Curtin <ecurtin@redhat.com> - 0.1.4-1
- Ensure writes are sync'd to disk

* Wed Jul 12 2023 Alexander Larsson <alexl@redhat.com> - 0.1.3-1
- Update ostree stages for userspace signature approach

* Thu Jun  1 2023 Alexander Larsson <alexl@redhat.com> - 0.1.1-1
- Update composefs.deploy stage for lazy signatures

* Wed May 31 2023 Alexander Larsson <alexl@redhat.com> - 0.1-2
- Fix description

* Wed May 24 2023 Alexander Larsson <alexl@redhat.com> - 0.1-1
- Initial version
